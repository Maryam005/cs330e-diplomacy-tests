#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = 'A Madrid Hold' 
        a = diplomacy_read(s)
        self.assertEqual(a, ['A','Madrid','Hold'])
        

    def test_read_2(self):
        s = 'B Barcelona Move Madrid' 
        a = diplomacy_read(s)
        self.assertEqual(a,['B','Barcelona','Move','Madrid'])
        
    def test_read_3(self):
        s = 'C London Support B' 
        a = diplomacy_read(s)
        self.assertEqual(a,['C','London','Support','B'])



    # ----
    # eval
    # ----

    def test_eval_1(self):
        list_of_actions= [['A','Madrid','Hold']]
        v = diplomacy_eval(list_of_actions)
        self.assertEqual(v, 'A Madrid')


    def test_eval_2(self):
        list_of_actions= [['A','Madrid','Hold'],['B', 'Barcelona', 'Move', 'Madrid']]
        v = diplomacy_eval(list_of_actions)
        self.assertEqual(v, 'A [dead]\nB [dead]')

    def test_eval_3(self):
        list_of_actions= [['A','Madrid','Hold'],['B', 'Barcelona', 'Move', 'Madrid'],['C', 'London', 'Support', 'B']]
        v = diplomacy_eval(list_of_actions)
        self.assertEqual(v, 'A [dead]\nB Madrid\nC London')

    def test_eval_4(self):
        list_of_actions= [['A','Madrid','Hold'],['B', 'Barcelona', 'Move', 'Madrid'],['C', 'London', 'Support', 'B'],['D', 'Austin', 'Move', 'London']]
        v = diplomacy_eval(list_of_actions)
        self.assertEqual(v, 'A [dead]\nB [dead]\nC [dead]\nD [dead]')

 
    # -----
    # print
    # -----

    def test_print1(self):
        w = StringIO()
        diplomacy_print(w, "A [dead]")
        self.assertEqual(w.getvalue(), "A [dead]\n")
        
    def test_print2(self):
        w = StringIO()
        diplomacy_print(w, "A Madrid\nB [dead]\nC London")
        self.assertEqual(w.getvalue(), "A Madrid\nB [dead]\nC London\n")

    def test_print3(self):
        w = StringIO()
        diplomacy_print(w, "A [dead]\nB [dead]\nC [dead]\nD [dead]")
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
                        
    # -----
    # solve
    # -----
    
    def test_solve_1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")
    
    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Rome Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\nD Rome\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Austin Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
    
# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
