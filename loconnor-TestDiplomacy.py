# 3 unit tests for Diplomacy_solve()

from io import StringIO
from unittest import main, TestCase

from diplomacy import diplomacy_solve

class TestDiplomacy (TestCase):

    def test1(self):
    	inp = "A Frankfurt Hold"
    	output = diplomacy_solve(inp)
    	check = "A Frankfurt"
    	self.assertEqual(output, check)

    def test2(self):
    	inp = "A Frankfurt Hold\nB Dublin Move Frankfurt\nC Dubai Support B"
    	output = diplomacy_solve(inp)
    	check = "A [dead]\nB Frankfurt\nC Dubai"
    	self.assertEqual(output, check)

    def test3(self):
    	inp = "A Frankfurt Hold\nB Dublin Move Frankfurt"
    	output = diplomacy_solve(inp)
    	check = "A [dead]\nB [dead]"
    	self.assertEqual(output, check)

    def test4(self):
    	inp = "A Frankfurt Hold\nB Dublin Move Frankfurt\nC Dubai Support B\nD Austin Move Dubai"
    	output = diplomacy_solve(inp)
    	check = "A [dead]\nB [dead]\nC [dead]\nD [dead]"
    	self.assertEqual(output, check)

    def test5(self):
    	inp = "A Frankfurt Hold\nB Dublin Move Frankfurt\nC Dubai Move Frankfurt\nD Paris Support B"
    	output = diplomacy_solve(inp)
    	check = "A [dead]\nB Frankfurt\nC [dead]\nD Paris"
    	self.assertEqual(output, check)

    def test6(self):
    	inp = "A Frankfurt Hold\nB Dublin Move Frankfurt\nC Dubai Move Frankfurt\nD Paris Support B\nE Austin Support A"
    	output = diplomacy_solve(inp)
    	check = "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin"
    	self.assertEqual(output, check)

# ----
# main
# ----
if __name__ == "__main__": #pragma: no cover
    main()

''' #pragma: no cover
coverage run TestDiplomacy.py
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
TestDiplomacy.py      34      0      0      0   100%
diplomacy.py          54      0     44      2    98%   57->56, 77->76
--------------------------------------------------------------
TOTAL                 88      0     44      2    98%
(base) Laurens-Air-2:diplomacy laurenoconnor$ coverage run TestDiplomacy.py
......
----------------------------------------------------------------------
Ran 6 tests in 0.001s

OK

coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1
cat TestDiplomacy.out
......
----------------------------------------------------------------------
Ran 6 tests in 0.001s

OK

coverage report -m
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
TestDiplomacy.py      34      0      0      0   100%
diplomacy.py          54      0     44      2    98%   57->56, 77->76
--------------------------------------------------------------
TOTAL                 88      0     44      2    98%

coverage report -m                   >> TestDiplomacy.out
cat TestDiplomacy.out
......
----------------------------------------------------------------------
Ran 6 tests in 0.001s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
TestDiplomacy.py      34      0      0      0   100%
diplomacy.py          54      0     44      2    98%   57->56, 77->76
--------------------------------------------------------------
TOTAL                 88      0     44      2    98%
''' 
